<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Desafio Z</title>
  </head>
  <body>

    @include('layouts.nav')

    <div class="container">
      @yield('content')
    </div>


  </body>
</html>
