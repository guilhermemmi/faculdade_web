<?php

namespace App\Http\Controllers;

class StudentsController extends Controller
{
    public function index()
    {
        return view('index');
    }

    public function login()
    {
        return view('student.login');
    }

    public function dashboard()
    {
        return view('student.dashstudent');
    }
}
