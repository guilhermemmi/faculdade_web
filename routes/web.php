<?php

Route::get('/', 'StudentsController@index');

Route::get('/login', 'StudentsController@login');

Route::get('/dashboard', 'StudentsController@dashboard');
